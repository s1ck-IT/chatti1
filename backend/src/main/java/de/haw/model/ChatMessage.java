package de.haw.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by dgreipl on 10.11.2015.
 */
@Entity
public class ChatMessage {
    @Id
    Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getA() {
        return A;
    }

    public void setA(String a) {
        A = a;
    }

    public String getAName() {
        return AName;
    }

    public void setAName(String AName) {
        this.AName = AName;
    }

    public String getPubKeyA() {
        return pubKeyA;
    }

    public void setPubKeyA(String pubKeyA) {
        this.pubKeyA = pubKeyA;
    }

    public String getB() {
        return B;
    }

    public void setB(String b) {
        B = b;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    @Index
    String A; // A-Party

    String AName; // Nickname von A

    String pubKeyA; // Public Key A

    @Index
    String B; // B-Party

    String message;

    @Index
    Long timestamp;

    Byte state;
}
