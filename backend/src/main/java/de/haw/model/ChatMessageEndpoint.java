package de.haw.model;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.android.gcm.server.Message;


import javax.annotation.Nullable;
import javax.inject.Named;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Api(
        name = "chatMessageApi",
        version = "v1",
        resource = "chatMessage",
        namespace = @ApiNamespace(
                ownerDomain = "model.haw.de",
                ownerName = "model.haw.de",
                packagePath = ""
        )
)
public class ChatMessageEndpoint {

    private static final String API_KEY = "AIzaSyDzK4xPMHaOhQ2ZNhbaJSyKHY-gbvZHtzk";
    private static final Logger logger = Logger.getLogger(ChatMessageEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(ChatMessage.class);
    }

    /**
     * Returns the {@link ChatMessage} with the corresponding ID.
     *
     * @param id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     * @throws NotFoundException if there is no {@code ChatMessage} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "chatMessage/{id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public ChatMessage get(@Named("id") Long id) throws NotFoundException {
        logger.info("Getting ChatMessage with ID: " + id);
        ChatMessage chatMessage = ofy().load().type(ChatMessage.class).id(id).now();
        if (chatMessage == null) {
            throw new NotFoundException("Could not find ChatMessage with ID: " + id);
        }
        return chatMessage;
    }

    /**
     * Inserts a new {@code ChatMessage}.
     */
    @ApiMethod(
            name = "insert",
            path = "chatMessage",
            httpMethod = ApiMethod.HttpMethod.POST)
    public ChatMessage insert(ChatMessage chatMessage) {

        ChatUser B;
        // Wir suchen nach B (Emfänger)
        B = ofy().load()
                .type(ChatUser.class)
                .id(Long.valueOf(chatMessage.getB()))
                .now();
        if (B == null) {
            // B gibt es nicht (mehr)
            chatMessage.setState(new Byte((byte) 99));
            logger.severe(" B-Party not found.");
            return chatMessage;
        }

        // Set the timestamp
        chatMessage.setTimestamp(new java.util.Date().getTime());

        ofy().save().entity(chatMessage).now();
        logger.info("Created ChatMessage with ID: " + chatMessage.getId());

        // Senden der Notification
        Message msg;
        msg = new Message.Builder()
                .addData("MESSAGE", chatMessage.getMessage())
                .addData("A", chatMessage.getAName())
                .build();
        Sender sender = new Sender(API_KEY);

        try {
            Result result = sender.send(msg, B.getDevice1(), 5);
            logger.warning("Insert Chatmessage : Sending notification to "
                    + B.getDevice1()
                    + "Result : " + result.getErrorCodeName()
                    + "Message ID : " + result.getMessageId()
                    + "Contents : " + msg.toString());
        } catch (IOException e) {
            logger.severe("  Notification failed  " + B.getDevice1());
            e.printStackTrace();
        }

        logger.warning("  Sent notification to " + B.getDevice1());

        return ofy().load().entity(chatMessage).now();
    }

    /**
     * Updates an existing {@code ChatMessage}.
     *
     * @param id          the ID of the entity to be updated
     * @param chatMessage the desired state of the entity
     * @return the updated version of the entity
     * @throws NotFoundException if the {@code id} does not correspond to an existing
     *                           {@code ChatMessage}
     */
    @ApiMethod(
            name = "update",
            path = "chatMessage/{id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public ChatMessage update(@Named("id") Long id, ChatMessage chatMessage) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(id);
        ofy().save().entity(chatMessage).now();
        logger.info("Updated ChatMessage: " + chatMessage);
        return ofy().load().entity(chatMessage).now();
    }

    /**
     * Deletes the specified {@code ChatMessage}.
     *
     * @param id the ID of the entity to delete
     * @throws NotFoundException if the {@code id} does not correspond to an existing
     *                           {@code ChatMessage}
     */
    @ApiMethod(
            name = "remove",
            path = "chatMessage/{id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("id") Long id) throws NotFoundException {
        checkExists(id);
        ofy().delete().type(ChatMessage.class).id(id).now();
        logger.info("Deleted ChatMessage with ID: " + id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "chatMessage",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<ChatMessage> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<ChatMessage> query = ofy().load().type(ChatMessage.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<ChatMessage> queryIterator = query.iterator();
        List<ChatMessage> chatMessageList = new ArrayList<ChatMessage>(limit);
        while (queryIterator.hasNext()) {
            chatMessageList.add(queryIterator.next());
        }
        return CollectionResponse.<ChatMessage>builder().setItems(chatMessageList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    @ApiMethod(
            name = "listMyChats",
            path = "chatMessage1",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<ChatMessage> listMyChats(@Named("B") String B) {
        List<ChatMessage> chatMessageList = new ArrayList<ChatMessage>();
        chatMessageList.addAll(
                ofy()
                        .load()
                        .type(ChatMessage.class)
                        .filter("B",B)
                        .order("-timestamp")
                        .limit(DEFAULT_LIST_LIMIT)
                        .list()
        );
        logger.warning("  ListMyChats : " + chatMessageList.size());
        return CollectionResponse.<ChatMessage>builder()
                .setItems(chatMessageList)
                .build();
    }

    private void checkExists(Long id) throws NotFoundException {
        try {
            ofy().load().type(ChatMessage.class).id(id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find ChatMessage with ID: " + id);
        }
    }
}