package de.haw.chatti;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import de.haw.chatti.core.Backend;
import de.haw.chatti.core.EndUser;
import de.haw.chatti.core.MyUtils;
import de.haw.model.chatMessageApi.model.ChatMessage;
import de.haw.model.chatMessageApi.model.CollectionResponseChatMessage;
import de.haw.model.chatUserApi.model.ChatUser;
import de.haw.model.chatUserApi.model.CollectionResponseChatUser;


public class MainActivity extends AppCompatActivity {

    private final static String TAG =  "MainActivity";
    ArrayAdapter<ChatMessage> mAdapter;
    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ListMessagesTask().execute();
            }
        });
        Log.d(TAG, "Main activity called.");

        // Init servcies to communicate w backend
        Backend.initEndpointServices(getApplicationContext());

        // Initialiseriung der EndUSerDaten
        EndUser.init( getApplicationContext() );

        // Adapter für die ListView
        mAdapter = new ArrayAdapter<ChatMessage>(this,
                android.R.layout.simple_list_item_2,
                android.R.id.text1,
                EndUser.getChatList()){

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View view = super.getView(position, convertView, parent);

                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                ChatMessage cm = EndUser.getChatList().get( position);

                text1.setText( cm.getAname() ); //Name des Senders
                text2.setText( cm.getMessage() + " (" + MyUtils.getFormattedTime(cm) +")"); //Nachricht

                return view;
            }
        };

        mListView = (ListView) findViewById( R.id.listViewMessages);
        mListView.setAdapter( mAdapter );
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                             @Override
                                             public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                 Intent intent;
                                                 intent = new Intent(getApplication(),
                                                         SendActivity.class);

                                                 // Transport von Werten zwischen activites
                                                intent.putExtra("TO", EndUser.getChatList().get(position).getA());

                                                 startActivity(intent);
                                                 Toast.makeText(
                                                         getApplicationContext(),
                                                         "Clicked on " + position,
                                                         Toast.LENGTH_LONG).show();

                                             }
                                         }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EndUser.isRegistered()){
            Log.i(TAG, "Not registered. Sent to registration");
            Intent intent;
            intent = new Intent(getApplication(),
                    RegisterActivity.class);
            startActivity( intent );
        }
        new ListMessagesTask().execute();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.id_settings) {
            Intent intent;
            intent = new Intent(getApplication(), SettingsActivity.class);
            startActivity( intent );
            return true;
        }

        if (id == R.id.id_contacts) {
            Intent intent;
            intent = new Intent(getApplication(), ContactsActivity.class);
            startActivity( intent );
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class ListMessagesTask extends AsyncTask<Void, Void, List<ChatMessage>> {
        @Override
        protected List<ChatMessage> doInBackground(Void... params) {
            CollectionResponseChatMessage response = null;
            try{
                response = Backend
                        .messageService
                        .listMyChats(EndUser.getRegId())
                        .execute();
                return (response==null) ? null : response.getItems();
            }
            catch (Exception e){
                Log.e(TAG, "Error receiving messages.");
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<ChatMessage> chatMessages) {
            super.onPostExecute(chatMessages);
            if (chatMessages == null){ // Failure

                // Keine User vorhanden, oder Fehler beim call
                Toast.makeText(getApplicationContext(),
                        "No messages received. On line?",
                        Toast.LENGTH_SHORT).show();

            } else { // Erfolg!
                // Lösche die die bisherige Userliste
                EndUser.getChatList().clear();

                // Fülle die Userliste mit den eben empfangene KOntakten
                EndUser.getChatList().addAll( chatMessages );

                Toast.makeText(getApplicationContext(),
                        "Received messages " + chatMessages.size(),
                        Toast.LENGTH_SHORT).show();
            }
            mAdapter.notifyDataSetChanged();
        }
    }
}
