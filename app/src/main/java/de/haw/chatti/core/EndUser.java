package de.haw.chatti.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import de.haw.model.chatMessageApi.model.ChatMessage;
import de.haw.model.chatUserApi.model.ChatUser;

/**
 * Created by dgreipl on 13.10.2015.
 */
public class EndUser {

    // Nicht persistente Chatlisten
    static List<ChatMessage> sentList = new ArrayList<ChatMessage>(); // gesendete Nachrichten
    static List<ChatMessage> chatList = new ArrayList<ChatMessage>();
    static List<ChatUser> userList = new ArrayList<ChatUser>();



    static public List<ChatMessage> getSentList() {
        return sentList;
    }


    static public List<ChatMessage> getChatList() {
        return chatList;
    }

    static SharedPreferences sharedPref;
    static SharedPreferences.Editor editor;

    // Meine Persistente Daten
    static String nickName;
    static String regId;
    static String Device1;
    static String email;
    static String pin;


    public static String getPin() {
        return pin;
    }

    public static void init( Context context){
        sharedPref = PreferenceManager.getDefaultSharedPreferences( context);
        editor = sharedPref.edit();
        loadEndUserData();
    }

    public static void loadEndUserData(){
        nickName = sharedPref.getString(Constants.PROPERTY_NICKNAME, "");
        regId = sharedPref.getString(Constants.PROPERTY_REGID, "");
        Device1 = sharedPref.getString(Constants.PROPERTY_DEVICE1, "");
        email = sharedPref.getString(Constants.PROPERTY_EMAIL, "");
        pin  = sharedPref.getString(Constants.PROPERTY_PIN, "");
    }

    public static void storeValue( String property, String value){
        editor.putString(property, value);
        editor.commit();
    }

    public static String getNickName() {
        return nickName;
    }


    public static String getRegId() {
        return regId;
    }



    public static String getDevice1() {
        return Device1;
    }

    public static String getEmail() {
        return email;
    }


    public static boolean isRegistered(){
        return !regId.isEmpty();
    }

    public static List<ChatUser> getContactList() {
        return userList;
    }
}
