package de.haw.chatti;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;;

import de.haw.chatti.core.Constants;
import de.haw.chatti.core.EndUser;

/**
 * Created by dgreipl on 22.12.2015.
 */
public class MyGCMIntentService extends IntentService {

    private final static String TAG = "MyGCMIntentService";
    NotificationManager mNotificationManager;

    public MyGCMIntentService() {
        super(Constants.PROJECTNUMBER);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Bundle extras = intent.getExtras();
        Log.d(TAG, " on Handle intent called with : " + extras.toString());

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        String messageType = gcm.getMessageType(intent);
        if (!extras.isEmpty()) {
            if (messageType.equals(GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE)) {
                sendNotification(
                        extras.getString("MESSAGE"),
                        extras.getString("A")
                );
            } else {
                sendNotification("CHATTI: Unknown messageType", extras.toString());
            }
        }
        else {
            sendNotification("CHATTI:  Message error ", "No extra data attached");
        }
    }

    private void sendNotification(String msg, String A) {
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Log.d(TAG, "A=" + A + " msg =" + msg);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(
                Intent.FLAG_ACTIVITY_SINGLE_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP
        );

        PendingIntent contentIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        NotificationCompat.Builder mBuilder;
        mBuilder = new NotificationCompat.Builder(this);

        mBuilder.setSmallIcon(R.drawable.ic_stat_name);
        mBuilder.setContentTitle("CHATTI-Message from " + A);
        mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
        mBuilder.setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(1, mBuilder.build());
    }
}
