package de.haw.chatti.core;

/**
 * Created by dgreipl on 13.10.2015.
 */
public class Constants {

    public static final String PROPERTY_NICKNAME = "nickname";
    public static final String PROPERTY_REGID = "regid";
    public static final String PROPERTY_DEVICE1 = "device1";
    public static final String PROPERTY_EMAIL = "email";
    public static final String PROPERTY_PIN = "pin";

    public static final String SUCCESS = "1";
    public static final String FAILURE = "0";

    public static final String PROJECTNUMBER = "732402633636";

}
