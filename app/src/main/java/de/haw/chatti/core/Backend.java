package de.haw.chatti.core;

import android.content.Context;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.io.IOException;

import de.haw.model.chatMessageApi.ChatMessageApi;
import de.haw.model.chatMessageApi.model.ChatMessage;
import de.haw.model.chatUserApi.ChatUserApi;


/**
 * Created by dgreipl on 13.10.2015.
 */
public class Backend {

    public static ChatMessageApi messageService = null;
    public static ChatUserApi userService = null;


    public static void initEndpointServices(Context ctx) {
        Log.d("Backend", "Generating Endpointservices");

        if (messageService == null) {

            ChatMessageApi.Builder endpointBuilder = new ChatMessageApi.Builder(
                    AndroidHttp.newCompatibleTransport(),
                    new JacksonFactory(),
                    new HttpRequestInitializer() {
                        @Override
                        public void initialize(HttpRequest httpRequest) throws IOException {

                        }
                    }
            );


            messageService = CloudEndpointUtils.updateBuilder(endpointBuilder).build();
        }

        if (userService == null) {

            ChatUserApi.Builder endpointBuilder = new ChatUserApi.Builder(
                    AndroidHttp.newCompatibleTransport(),
                    new JacksonFactory(),
                    new HttpRequestInitializer() {
                        @Override
                        public void initialize(HttpRequest httpRequest) throws IOException {

                        }
                    }
            );

            userService = CloudEndpointUtils.updateBuilder(endpointBuilder).build();
        }
    }


    public static void initTestData() {
        ChatMessage cm = new ChatMessage();
        cm.setA("2");
        cm.setAname("Herr A");
        cm.setB("1");
        cm.setMessage("Hallo B");
        cm.setTimestamp(Long.valueOf(new java.util.Date().getTime()));

        EndUser.getChatList().add(cm);
        EndUser.getChatList().add(cm);
    }
}
