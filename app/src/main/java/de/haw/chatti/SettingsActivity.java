package de.haw.chatti;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import de.haw.chatti.core.Backend;
import de.haw.chatti.core.Constants;
import de.haw.chatti.core.EndUser;

public class SettingsActivity extends AppCompatActivity
implements  SettingsFragment.OnFragmentInteractionListener {

    private static final String TAG = "SettingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getFragmentManager().beginTransaction()
                .replace
                        (R.id.main_content_layout,
                        new SettingsFragment()
                )
                .commit();
    }

    public void onFragmentInteraction(String command){
        if (command.equals("UNREGISTER")){
            new UnregisterTask().execute();
        }
    }



    private class UnregisterTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {

            try {
                // Call to backend
                Backend.userService
                        .remove(Long.valueOf(EndUser.getRegId()))
                        .execute();
                return Constants.SUCCESS;

            } catch (Exception e) {

                Log.e(TAG, "   Unregistration failed.");
                e.printStackTrace();
                return Constants.FAILURE;
            }

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equals(Constants.SUCCESS)) {

                EndUser.storeValue(Constants.PROPERTY_REGID, "");
                EndUser.loadEndUserData();
                Toast.makeText(getApplicationContext(),
                        "Your account has been deleted.",
                        Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Unregistration failed! On-line?",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }
}
