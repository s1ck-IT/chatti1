package de.haw.chatti.core;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import de.haw.model.chatMessageApi.model.ChatMessage;

/**
 * Created by dgreipl on 13.10.2015.
 */
public class MyUtils {

    public static String getFormattedTime( ChatMessage message){
        String s = "";
        if (message == null) return "<Date missing>";
        if (message.getTimestamp()==null) return "<Date missing>";

        // A good link: http://www.mkyong.com/java/java-date-and-calendar-examples/


        long lNow = new java.util.Date().getTime();
        long lMsg = message.getTimestamp().longValue();


        Calendar calMsg = new GregorianCalendar();
        Calendar calNow = new GregorianCalendar();

        calMsg.setTimeInMillis( lMsg);
        calNow.setTimeInMillis( lNow);

        boolean sameDay = calMsg.get(Calendar.YEAR) == calNow.get(Calendar.YEAR) &&
                calMsg.get(Calendar.DAY_OF_YEAR) == calNow.get(Calendar.DAY_OF_YEAR);



        // Are we today
        if ((lNow-lMsg)/1000.0 < 60){
            return "moments ago";
        } else if (sameDay){
            Date date = new Date();
            date.setTime( lMsg );
            SimpleDateFormat df2 = new SimpleDateFormat("HH:mm");
            s += df2.format(date);
            return "today, "+ s;
        }
        else{
            Date date = new Date();
            date.setTime( lMsg );
            SimpleDateFormat df2 = new SimpleDateFormat("dd.mm.yyyy HH:mm");
            s += df2.format(date);
            return s;
        }
    }
}
