package de.haw.chatti;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import de.haw.chatti.core.Backend;
import de.haw.chatti.core.EndUser;
import de.haw.model.chatUserApi.model.ChatUser;
import de.haw.model.chatUserApi.model.CollectionResponseChatUser;


public class ContactsActivity extends AppCompatActivity {

    private static final String TAG = "ContacsActivity";
    ArrayAdapter<ChatUser> mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView l = (ListView) findViewById(R.id.listViewContacts);
        mAdapter = new ArrayAdapter<ChatUser>(this,
                android.R.layout.simple_list_item_2, android.R.id.text1,
                EndUser.getContactList()) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = (TextView) view
                        .findViewById(android.R.id.text1);
                TextView text2 = (TextView) view
                        .findViewById(android.R.id.text2);

                ChatUser user = EndUser.getContactList().get(position);

                text1.setText(user.getNickName());
                text2.setText(user.getEmail());
                return view;
            }
        };

        l.setAdapter(mAdapter);
        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view,
                                    int position, long arg) {

                Intent intent = new Intent(getApplication(), SendActivity.class);
                intent.putExtra("TO", EndUser.getContactList().get(position).getId().toString());
                startActivity(intent);
            }
        });



    }

    protected void onStart() {
        super.onStart();
        // Lade die Liste der User immer, wenn diese Activtiy gestartet wird
        new ListUsersTask().execute();
    }


    private class ListUsersTask extends AsyncTask<Void, Void, List<ChatUser>>{
        @Override
        protected List<ChatUser> doInBackground(Void... params) {
            CollectionResponseChatUser response = null;
            try{
                response = Backend.userService.list().execute();
                return (response==null) ? null : response.getItems();
            }
            catch (Exception e){
                Log.e(TAG, "Error receiving users");
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<ChatUser> chatUsers) {
            super.onPostExecute(chatUsers);
            if (chatUsers == null){ // Failure

                // Keine User vorhanden, oder Fehler beim call
                Toast.makeText(getApplicationContext(),
                        "Did not receveive any user. On line?",
                        Toast.LENGTH_SHORT).show();

            } else { // Erfolg!
                // Lösche die die bisherige Userliste
                EndUser.getContactList().clear();

                // Fülle die Userliste mit den eben empfangene KOntakten
                EndUser.getContactList().addAll( chatUsers );

                Toast.makeText(getApplicationContext(),
                        "Received some users",
                        Toast.LENGTH_SHORT).show();
            }
            mAdapter.notifyDataSetChanged();
        }
    }
}
