package de.haw.chatti;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

/**
 * Created by dgreipl on 22.12.2015.
 */
public class MyReceiver extends WakefulBroadcastReceiver {



    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("MYRECEIVER", "Reveiver called");
        ComponentName comp;
        comp = new ComponentName(
                context.getPackageName(),
                MyGCMIntentService.class.getName());
        startWakefulService(context, intent.setComponent(comp));
        setResultCode(Activity.RESULT_OK);
    }
}
