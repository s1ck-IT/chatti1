package de.haw.chatti;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import de.haw.chatti.core.Backend;
import de.haw.chatti.core.Constants;
import de.haw.chatti.core.EndUser;
import de.haw.model.chatMessageApi.model.ChatMessage;
import de.haw.model.chatUserApi.model.ChatUser;

public class SendActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void doSend( View view){
        ChatMessage cm = new ChatMessage();
        cm.setA(EndUser.getRegId());
        cm.setB( getIntent().getStringExtra("TO"));
        cm.setAname(EndUser.getNickName());
        cm.setTimestamp( Long.valueOf( System.currentTimeMillis() ) );

        String message;
        message = ((EditText) findViewById (R.id.editTextMessage )).getText().toString();
        cm.setMessage( message );

        new SendTask().execute( cm );
    }

    private class SendTask extends AsyncTask<ChatMessage, Void, ChatMessage> {
        @Override
        protected ChatMessage doInBackground(ChatMessage... chatMessage) {

            ChatMessage response = null;

            try {
                // Call to backend to tore the message
                response = (ChatMessage) Backend
                        .messageService
                        .insert( chatMessage[0] )
                        .execute();
            } catch (Exception e) {
                Log.e("SENDTASK : ", "   Send failed! On-Line?");
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(ChatMessage chatMessage) {
            super.onPostExecute(chatMessage);
            if (chatMessage != null) {
                Toast.makeText(getApplicationContext(),
                        "Message sent.",
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Sending failed. On-line?",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}
