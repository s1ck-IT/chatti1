#CHATTI - RED GROUP (GKW)#

This is the repository for the GKW group starting on OCT-10. The tagged commits (V1, V2,...) show the code at the end of each session. 

**Session 1 (OCT-13)**  Source code at end of session has tag V1

**Session 2 (OCT-27)** Source code at end of session has tag V2

**Session 3 (NOV-10)** Source code at end of session has tag 3.1

**Session 4 (NOV-24)** Source code at end of session has tag V4.1 
(For preparation of next session implement all commits after this tag)

**Session 5 (Dec-8)** Source code at end of session has tag V5 
(For preparation of next session implement all commits after this tag)

**Session 6/final (Dec-22)**  
Notes:

1. V6 contains the state after the lecture @ Dec-22.  
   Pls. note spelling of "play-services" in the manifest file.

2. I added commits after V6 to 

* 6.1: make the notificaton work (needed some upades)
* 6.2: make the feature "pls send only my messages" work.

Pls update your code, make it run and test it. 

Merry X-mas