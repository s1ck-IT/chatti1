package de.haw.chatti;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import de.haw.chatti.core.Backend;
import de.haw.chatti.core.Constants;
import de.haw.chatti.core.EndUser;
import de.haw.model.chatUserApi.model.ChatUser;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }


    public void doRegister(View view) {
        // call the AsyncTask
        new RegisterTask().execute();
    }

    public ChatUser createChatUser() {
        ChatUser cu = new ChatUser();
        String nickName, pin, email;

        // Set nickname
        nickName = ((EditText) findViewById(R.id.editTextNicknam)).getText().toString();
        cu.setNickName(nickName);

        // Set pin
        pin = ((EditText) findViewById(R.id.editTextPin)).getText().toString();
        cu.setPin(pin);

        // Set E-mail
        email = ((EditText) findViewById(R.id.editTextMail)).getText().toString();
        cu.setEmail(email);

        return cu;

    }


    private class RegisterTask extends AsyncTask<Void, Void, ChatUser> {
        @Override
        protected ChatUser doInBackground(Void... params) {
            // Create user to be registered
            ChatUser newUser = createChatUser();

            // Contains response form backend
            ChatUser response = null;

            try {
                // Get GCM Messaging ID
                GoogleCloudMessaging gcm;
                gcm = GoogleCloudMessaging
                        .getInstance( getApplicationContext());

                String gcmID;
                gcmID = gcm.register( Constants.PROJECTNUMBER);
                newUser.setDevice1( gcmID);

                Log.d(TAG, "   GCM ID = " + gcmID);

                // Call to backend
                response = (ChatUser) Backend.userService.insert(newUser).execute();
            } catch (Exception e) {
                Log.e(TAG, "   Registration failed.");
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(ChatUser chatUser) {
            super.onPostExecute(chatUser);
            if (chatUser != null) {

                // Store data in preferences
                EndUser.storeValue(Constants.PROPERTY_REGID, chatUser.getId().toString() );
                EndUser.storeValue(Constants.PROPERTY_NICKNAME, chatUser.getNickName());
                EndUser.storeValue(Constants.PROPERTY_PIN, chatUser.getPin());
                EndUser.storeValue(Constants.PROPERTY_EMAIL, chatUser.getEmail());
                EndUser.loadEndUserData();
                Toast.makeText(getApplicationContext(),
                        "You are registered now.",
                        Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Registration failed. On-line?",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}
